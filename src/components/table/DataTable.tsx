import React from 'react';
import {
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Box
} from '@chakra-ui/react';

const DataTable = ({ columns, data }) => {
    return (
        <Box w="full" overflowX="auto">
            <Table w="full" variant="simple">
                <Thead>
                    <Tr>
                        {columns.map((column, index) => (
                            <Th key={index}>{column.header}</Th>
                        ))}
                    </Tr>
                </Thead>
                <Tbody>
                    {data.map((row, rowIndex) => (
                        <Tr key={rowIndex}>
                            {columns.map((column, colIndex) => {
                                const value = row[column.accessorKey];
                                // Check if a custom Cell renderer is provided and if the value is not undefined
                                const Cell = column.Cell;
                                return (
                                    <Td key={colIndex}>
                                        {Cell ? <Cell value={value} row={row} /> : value !== undefined ? value : 'N/A'}
                                    </Td>
                                );
                            })}
                        </Tr>
                    ))}
                </Tbody>
            </Table>
        </Box>
    );
};

export default DataTable;
