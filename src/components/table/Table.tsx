import React, { useState } from 'react'
import { createColumnHelper, flexRender, getCoreRowModel, useReactTable } from '@tanstack/react-table'
import {Button, Flex, Icon, IconButton, Table, Td, Text, Tfoot, Th, Thead, Tr} from '@chakra-ui/react'
import { ChevronDownIcon, ChevronLeftIcon, ChevronRightIcon, ChevronUpIcon, UpDownIcon } from '@chakra-ui/icons';
interface IColumn{
    header: string;
    accessor: string;
    cell?: (data: unknown) => unknown;
    getRowData?: boolean;
    size?: number;

}
const DataTable = ({data,columns}) => {

  const [sortedData, setSortedData] = useState(data);
  const [isAscending, setIsAscending] = useState(true);

    const table = useReactTable({
        data: sortedData,
        columns,
        getCoreRowModel: getCoreRowModel(),
      });


  const sortColumn = (columnName, columnType) => {
    let sorted;
    if (columnType === 'string') {
      sorted = [...sortedData].sort((a, b) => {
        if (isAscending) {
          return a[columnName].localeCompare(b[columnName]);
        } else {
          return b[columnName].localeCompare(a[columnName]);
        }
      });
    } else if (columnType === 'date') {
      sorted = [...sortedData].sort((a, b) => {
        if (isAscending) {
          return new Date(a[columnName]) - new Date(b[columnName]);
        } else {
          return new Date(b[columnName]) - new Date(a[columnName]);
        }
      });
    }
    setSortedData(sorted);
    setIsAscending(!isAscending); // Toggle sorting direction
  };

      return (
        <div className="p-2">
          <Table>
            <Thead>
              {table.getHeaderGroups().map(headerGroup => (
                <Tr bg="gray.100" m="4" key={headerGroup.id}>
                  {headerGroup.headers.map(header => (
                    <Th color="gray.400" key={header.id}>
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                            
                          )}
                          {/*  Don't apply sorting method to "Status" and "Actions" columns */}
                          {header.column.id !== "status" && header.column.id !== "actions" && (
                      <>
                         {/*  icon for sorting */}
                        <UpDownIcon 
                          _hover={{ color: "#2563EB" }}
                          color="gray.900"
                          onClick={() => sortColumn(header.column.id, typeof data[0][header.column.id])}
                          aria-label={''} 
                          ml="8"
                        />
                      </>
                      )}
                    </Th>
                  ))}
                </Tr>
              ))}
            </Thead>
            <tbody>
              {table.getRowModel().rows.map(row => (
                <Tr key={row.id}>
                  {row.getVisibleCells().map(cell => (
                    <Td fontSize="md" fontWeight="semibold" key={cell.id}>
                      {flexRender(cell.column.columnDef.cell, cell.getContext())}
                    </Td>
                  ))}
                </Tr>
              ))}
            </tbody>
            <Tfoot>
              {table.getFooterGroups().map(footerGroup => (
                <Tr key={footerGroup.id}>
                  {footerGroup.headers.map(header => (
                    <Th key={header.id}>
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.footer,
                            header.getContext()
                          )}
                    </Th>
                  ))}
                </Tr>
              ))}
            </Tfoot>
          </Table>
          {/* Below table */}
      <Flex justify="space-between" align="center" mt="4">
        {/* Dropdown to choose number of items */}
        <Flex align="center">
          <Text fontSize="md" fontWeight="semibold" mr="4">
            Show Rows
          </Text>
          <Button
            _hover={{ color: "#2563EB" }}
            color="gray.500"
            border="1px"
            borderColor="gray.200"
            variant="ghost"
            rightIcon={<Icon as={ChevronDownIcon} />}
            mr="2"
          >
            10 items
          </Button>
        </Flex>
        {/* list of pages */}
        <Flex>
          <IconButton
            _hover={{ color: "#2563EB" }}
            border="1px"
            borderColor="gray.200"
            variant="ghost"
            mr="4"
            icon={<ChevronLeftIcon />}
            aria-label={''} 
          />

          <Button _hover={{ color: "#2563EB" }} border="1px" borderColor="gray.200" variant="ghost" mr="2">
            1
          </Button>
          <Button _hover={{ color: "#2563EB" }} border="1px" borderColor="gray.200" variant="ghost" mr="2">
            2
          </Button>
          <Button _hover={{ color: "#2563EB" }} border="1px" borderColor="gray.200" variant="ghost" mr="2">
            3
          </Button>
          <IconButton
                _hover={{ color: "#2563EB" }}
                border="1px"
                borderColor="gray.200"
                mr="2"
                variant="ghost"
                ml="4"
                icon={<ChevronRightIcon />} 
                aria-label={''}          />
        </Flex>
      </Flex>
        
        </div>
      )
}

export default DataTable