//ProjectDetails.tsx
import React from 'react';
import { Box, Button, Divider, Flex, Grid } from '@chakra-ui/react';
import { ChevronRightIcon, ChevronLeftIcon } from '@chakra-ui/icons';
import { Link } from '@tanstack/react-router';

interface ProjectDetailsProps {
    projectName: string;
    projectId: string;
    estimatedStartDate: string;
    estimatedEndDate: string;
    description: string;
}


const ProjectDetails: React.FC<ProjectDetailsProps> = ({
                                                           projectName,
                                                           projectId,
                                                           estimatedStartDate,
                                                           estimatedEndDate,
                                                           description,
                                                       }) => {
    // Define props with their respective values

    return (
        <>
            {/* Start */}
            <Box mb={4} display="flex" alignItems="center" justifyContent="space-between" >
                {/* Left part containing project icon, name, and information */}
                <Box display="flex" alignItems="center">
                    <ChevronRightIcon mr={4} boxSize={36} color="blue" />
                    <Box>
                        <Box fontSize="2xl" fontWeight="bold">{projectName}</Box>
                        <Box fontSize="sm" color="gray.500" fontWeight="semibold">Get more details related to this specific Project:</Box>
                    </Box>
                </Box>
                {/* Right part containing the button */}
                <Link to="/projets">
                    <Button colorScheme="blue" leftIcon={<ChevronLeftIcon />}>Return to Previous Page</Button>
                </Link>
            </Box>
            {/* This Project details */}
            <Grid mb={4}>
                <Flex alignItems="center" gap={4} bg="gray.50" px="6" pt="2" pb="2" rounded="xl">
                    {/* Info element 1 */}
                    <Box>
                        <Box whiteSpace="nowrap" fontWeight="semibold" color="gray.400">User ID (ClickUp)</Box>
                        <Box whiteSpace="nowrap" fontWeight="bold">{projectName}</Box>
                    </Box>
                    <Divider orientation="vertical" borderColor="blue.400" mx={0} my={0} />
                    {/* Info element 2 */}
                    <Box>
                        <Box whiteSpace="nowrap" fontWeight="semibold" color="gray.400">Estimated Start Date</Box>
                        <Box whiteSpace="nowrap" fontWeight="bold">{estimatedStartDate}</Box>
                    </Box>
                    <Divider orientation="vertical" borderColor="blue.400" />
                    {/* Info element 3 */}
                    <Box>
                        <Box whiteSpace="nowrap" fontWeight="semibold" color="gray.400">Estimated End Date</Box>
                        <Box whiteSpace="nowrap" fontWeight="bold">{estimatedEndDate}</Box>
                    </Box>
                    <Divider orientation="vertical" borderColor="blue.400" mx={0} my={0} />
                    {/* Info element 4 */}
                    <Box>
                        <Box whiteSpace="nowrap" fontWeight="semibold" color="gray.400">Description</Box>
                        <Box whiteSpace="nowrap" fontWeight="bold">{description}</Box>
                    </Box>
                </Flex>
            </Grid>
            <Box h=""></Box>
            {/* End */}
        </>
    );
};

export default ProjectDetails;
