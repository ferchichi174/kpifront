import React, { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import axios from 'axios';

const ProjectChart = () => {
    const [chartData, setChartData] = useState({
        labels: [],
        datasets: [{
            label: 'Task Count per Project',
            data: [],
            backgroundColor: 'rgba(75, 192, 192, 0.6)',
        }]
    });

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/projects');
                const data = response.data;
                const labels = data.map(project => project.name);
                const taskData = data.map(project => parseInt(project.task_count));

                setChartData({
                    labels: labels,
                    datasets: [{
                        label: 'Task Count per Project',
                        data: taskData,
                        backgroundColor: 'rgba(75, 192, 192, 0.6)',
                    }]
                });
            } catch (error) {
                console.error('Error fetching data: ', error);
            }
        };

        fetchData();
    }, []);

    return (
        <div>
            <h2>Project Task Count Overview</h2>
            <Bar data={chartData} options={{
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }} />
        </div>
    );
}

export default ProjectChart;
