import React, { useState } from 'react';
import {
  FormControl,
  FormLabel,
  Input,
  Textarea,
  Select,
  FormErrorMessage,
  InputProps,
} from '@chakra-ui/react';

interface FormInputProps extends InputProps {
  label?: string;
  type: 'text' | 'date' | 'select' | 'textArea';
  options?: string[];
}

const FormInput: React.FC<FormInputProps> = ({ label, type, options, ...props }) => {
  const [value, setValue] = useState('');
  const [error, setError] = useState('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>) => {
    setValue(event.target.value);
  };

  const validate = () => {
    if (type === 'text') {
      if (!value.trim()) {
        setError(`${label} is required`);
        return false;
      }
    }
    setError('');
    return true;
  };

  const handleBlur = () => {
    validate();
  };

  return (
      <FormControl isInvalid={!!error}>
        <FormLabel>{label}</FormLabel>
        {type === 'text' && (
            <>
              <Input
                  value={value}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  {...props}
              />
              {error && <FormErrorMessage>{error}</FormErrorMessage>}
            </>
        )}
        {type === 'date' && (
            <>
              <Input
                  type="date"
                  value={value}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  {...props}
              />
              {error && <FormErrorMessage>{error}</FormErrorMessage>}
            </>
        )}
        {type === 'select' && (
            <Select value={value} onChange={handleChange} {...props}>
              {options?.map(option => (
                  <option key={option} value={option}>
                    {option}
                  </option>
              ))}
            </Select>
        )}
        {type === 'textArea' && (
            <>
              <Textarea
                  value={value}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  {...props}
              />
              {error && <FormErrorMessage>{error}</FormErrorMessage>}
            </>
        )}
      </FormControl>
  );
};

export default FormInput;
