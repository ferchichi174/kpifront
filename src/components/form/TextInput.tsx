import {
  FormLabel,
  Input,
  InputGroup,
  InputLeftElement,
  InputProps,
  InputRightElement,
  Text,
  VStack,
} from "@chakra-ui/react";
import { FC } from "react";
import { BaseSchema, BaseSchemaAsync } from "valibot";

interface FieldProps {
  children: ({ name, state, handleChange, handleBlur }) => React.ReactNode;
  name: string;
  validators?: {
    onChange?: BaseSchema<unknown, unknown> | BaseSchemaAsync<unknown, unknown>;
    onBlur?: BaseSchema<unknown, unknown> | BaseSchemaAsync<unknown, unknown>;
    onSubmit?: BaseSchema<unknown, unknown> | BaseSchemaAsync<unknown, unknown>; // Update the type of onSubmit property
  };
}
interface TextInputProps extends InputProps {
  name: string;
  label?: string;
  leftIcon?: React.ReactElement;
  rightIcon?: React.ReactElement;
  validators?: {
    onChange?: BaseSchema<unknown, unknown> | BaseSchemaAsync<unknown, unknown>;
    onBlur?: BaseSchema<unknown, unknown> | BaseSchemaAsync<unknown, unknown>;
    onSubmit?: BaseSchema<unknown, unknown> | BaseSchemaAsync<unknown, unknown>; // Update the type of onSubmit property
  };
  Field: FC<FieldProps>;
}

const TextInput: FC<TextInputProps> = ({
  Field,
  name,
  label,
  validators = {},
  leftIcon,
  rightIcon,
  ...props
}) => {
  return (
    <Field name={name} validators={validators}>
      {({ name: inputName, state, handleChange, handleBlur }) => (
        <VStack alignItems="start">
       {label &&    <FormLabel my="0" htmlFor={name}>
            {label}
          </FormLabel>}
          <InputGroup>
          {leftIcon && (
              <InputLeftElement
                children={leftIcon} // Adjust the color of the icon as needed
              />
            )}
            {rightIcon && (
              <InputRightElement
                children={rightIcon} // Adjust the color of the icon as needed
              />
            )}
            
            <Input
              type="text"
              id={name}
              {...props}
              name={inputName}
              defaultValue={state.value}
              onChange={(e) => handleChange(e.target.value)}
              onBlur={handleBlur}
            />
          </InputGroup>
          <Text fontSize="sm" color="red">
            {state.meta.touchedErrors ? (
              <em>{state.meta.touchedErrors}</em>
            ) : null}
          </Text>
        </VStack>
      )}
    </Field>
  );
};

export default TextInput;
