import { useForm } from "@tanstack/react-form";
import { valibotValidator } from "@tanstack/valibot-form-adapter";
import { BaseSchema, BaseSchemaAsync } from "valibot";

interface IFormOptions {
    defaultValues: Record<string, unknown>;
    onSubmit: (values: Record<string, unknown>) => void;
    validators?:{
        onChange?: BaseSchema<unknown, unknown> | BaseSchemaAsync<unknown, unknown>;
        onBlur?: BaseSchema<unknown, unknown> | BaseSchemaAsync<unknown, unknown>;
        onSubmit?: BaseSchema<unknown, unknown> | BaseSchemaAsync<unknown, unknown>; // Update the type of onSubmit property
    }
}


const useValitBotForrm = (formOptions: IFormOptions) => {
    const form = useForm({
        validatorAdapter: valibotValidator,
        ...formOptions,
    });
    return form;
};

export default useValitBotForrm;
