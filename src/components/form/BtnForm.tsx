import { Button, ButtonProps } from "@chakra-ui/react";
import React, {  FC } from "react";
interface IState {
  canSubmit: boolean;
  isSubmitting: boolean;
}
interface SubscribeProps {
  children: ([canSubmit, isSubmitting]: [boolean, boolean]) => React.ReactNode;
  selector: (state: IState) => [boolean, boolean];
}
interface BtnFormProps extends ButtonProps {
  name: string;
  Subscribe: FC<SubscribeProps>;
  children?: React.ReactNode;
  handleSubmit: (e: unknown) => void;



}
const BtnForm: FC<BtnFormProps> = ({ Subscribe,children ,handleSubmit, ...props}) => {
  return (
    <Subscribe selector={(state) => [state.canSubmit, state.isSubmitting]}>
      {([canSubmit, isSubmitting]) => (
        <Button isDisabled={!canSubmit} 
        colorScheme="blue" 
        onClick={handleSubmit} 
        
        isLoading={isSubmitting}
        {...props}
        >
         {children ? children:"Submit"} 

        </Button>
      )}
    </Subscribe>
  );
};

export default BtnForm;
