
import { theme } from '@chakra-ui/react'

const colors: { [color: string]: { [opacity: number]: string } } = {
  ...theme.colors,
  primary: {
    50: '#F6F5FD',
    100: '#E3DFF9',
    200: '#D0CAF5',
    300: '#BDB5F1',
    400: '#AAA0ED',
    500: '#4D37D9',
    600: '#8576E5',
    700: '#7261E1',
    800: '#5F4CDD',
    900: '#2C2C2C',
  },
  blue: {
    50: "#e7f1ff",
    100: "#c1d9ff",
    200: "#97bfff",
    300: "#6e9fe5",
    400: "#4d7fcc",
    500: "#2563eb",
    600: "#1b4aba",
    700: "#103487",
    800: "#06265f",
    900: "#00163a"
  },

}



export default colors
