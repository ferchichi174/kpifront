import { extendTheme } from "@chakra-ui/react";

import colors from "./colors";


export const theme = (dir?: string) => {
  return extendTheme({
    direction: dir,
    colors,

    styles: {
      global: {
        html: {
          fontSize: "14px",
        },
      },
    },

  });
};
