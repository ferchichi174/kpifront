import {
  Box,
  Flex,
  IconButton,
  Input,
  InputGroup,
  InputLeftElement,
  useColorMode,
} from "@chakra-ui/react";
import {
  SearchIcon,
  SunIcon,
  MoonIcon
} from "@chakra-ui/icons";
import { FaBell, FaRegUserCircle } from "react-icons/fa";

function Navbar() {
  const { colorMode, toggleColorMode } = useColorMode();

  return (
    <Box borderBottom="1px solid" borderColor="gray.200" p={4}>
      <Flex justify="space-between" align="center">

        <Flex mt="2" align="start" flex="1">
          <InputGroup>
            <InputLeftElement pointerEvents="none">
              <SearchIcon color="#CBD5E0" />
            </InputLeftElement>
            <Input type="search" placeholder="Search anything.." />
          </InputGroup>
          <IconButton
            aria-label="Notification"
            icon={<FaBell />}
            size="md"
            variant="ghost"
            color="gray.400"
            ml={2}
            _hover={{ color: "#2563EB" }}
          />
          <IconButton
            aria-label="User"
            icon={<FaRegUserCircle />}
            size="md"
            variant="ghost"
            color="#2563EB"
            ml={2}
          />
          <IconButton
            icon={colorMode === "light" ? <MoonIcon /> : <SunIcon />}
            onClick={toggleColorMode}
            aria-label="Toggle Dark Mode"
            variant="ghost"
            color="#2563EB"
            ml={2}
          />
        </Flex>
      </Flex>
  
    </Box>
  );
}

export default Navbar;
