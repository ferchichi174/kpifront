import React from 'react'
import { Box, Button, Flex, Icon, Image, Text } from '@chakra-ui/react'
import { FaCog, FaSignOutAlt } from 'react-icons/fa';
import { AiOutlineInfoCircle, AiOutlinePieChart, AiOutlineProject, AiOutlineShopping, AiOutlineUser } from "react-icons/ai";
import logo from '../../images/logo.png'

const SideBar = () => {


  const handleLogout = () => {
    localStorage.removeItem('token'); // Supprime le token de localStorage
    window.location.href = '/';
  }
  return (
      <div>
        <Box w="200px"  p="6" textAlign="left">
          <Flex direction="column">
            <Flex align="center" mb="20" >
              <Image src={logo} alt="KPI Project Logo" boxSize="32px" mr="2" />
              <Text fontWeight="bold" color="gray.700" >KPI Project</Text>
            </Flex>

            <Flex align="center" mb="4" >
              <Icon color="gray.600" as={AiOutlineInfoCircle} mr="4" />
              <Text color="gray.500" fontSize="md" fontWeight="semibold" >Overview</Text>
            </Flex>

            <Flex _hover={{ color: "#2563EB" }} align="center" mb="4" >
              <Icon color="gray.600" as={AiOutlinePieChart} mr="4" />
              <Text color="gray.500" fontSize="md" fontWeight="semibold" >Analytics</Text>
            </Flex>

            <Flex align="center" mb="4" >
              <Icon color="gray.600" as={AiOutlineShopping} mr="4" />
              <Text color="gray.500" fontSize="md" fontWeight="semibold" >Orders</Text>
            </Flex>

            <Flex align="center" mb="4" >
              <Icon color="gray.600" as={AiOutlineProject} mr="4" />
              <Text color="gray.500" fontSize="md" fontWeight="semibold" >Projects</Text>
            </Flex>

            <Flex  align="center" mb="4" >
              <Icon color="gray.600" as={AiOutlineUser} mr="4" />
              <Text color="gray.500" fontSize="md" fontWeight="semibold" >Customers</Text>
            </Flex>

            <Box textAlign="left" mt="36">
              <Button  _hover={{ color: "#2563EB" }} fontSize="md" color="gray.500" variant="ghost" leftIcon={<FaCog />} p="1" mr="2">
                Settings
              </Button>
            </Box>

            <Box>
              <Button
                  _hover={{ color: "#2563EB" }}
                  fontSize="md"
                  color="gray.500"
                  variant="ghost"
                  leftIcon={<FaSignOutAlt />}
                  p="1"
                  mr="2"
                  onClick={handleLogout}  // Ajoutez un gestionnaire de clic pour se déconnecter
              >
                Log Out
              </Button>
            </Box>
          </Flex>
        </Box>
      </div>
  )
}

export default SideBar