import { Outlet } from "@tanstack/react-router";
import React from "react";

const Container = () => {
  return (
    <div>
      <Outlet />
    </div>
  );
};

export default Container;
