import { Flex, VStack } from "@chakra-ui/react";
import React from "react";
import SideBar from "./components/SideBar";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Container from "./components/Container";

const Layout = () => {
  return (
    <Flex>
      <SideBar />
      <VStack>
        <Navbar />
        <Container />
        <Footer/>
      </VStack>
    </Flex>
  );
};

export default Layout;
