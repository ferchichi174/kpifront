import { queryOptions } from "@tanstack/react-query";
import { getProjects, addProject } from "./projects.service";

//getProjects
 const projectsQueries =()=> queryOptions({
    queryKey:['projects'],
    queryFn: getProjects
})

 // addProject
  const addProjectQuery = () => queryOptions({
    queryKey: ['addProject'],
    queryFn: addProject
  });


export  {projectsQueries, addProjectQuery };


