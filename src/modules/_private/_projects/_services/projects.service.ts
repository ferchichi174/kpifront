import apisService from "../../../../app/axios";
import { IProjectsResponse } from "./response.type";

// getProjects
const getProjects = async (): Promise<any> => {
  try {
    const response = await apisService.get('projects/getProject');
    return response.data;
  } catch (error) {
    // Handle error
    throw new Error("Failed to retrieve projects");
  }
}

//addProject
const addProject = async (): Promise<any> => {
  try {
    const response = await apisService.post('projects/addProject');
    return response.data;
  } catch (error) {
    // Handle error
    throw new Error("Failed to add project");
  }
}




export { getProjects, addProject}