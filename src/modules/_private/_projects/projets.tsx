//projets.tsx
import { createFileRoute, Link, useNavigate } from "@tanstack/react-router";
import DataTable from "../../../components/table/Table";
import { Box, Button, Divider, Flex, Grid, Icon, IconButton, Input, InputGroup, InputLeftElement, Text } from "@chakra-ui/react";
import { CheckCircleIcon, ChevronDownIcon, ChevronLeftIcon, ChevronRightIcon, DeleteIcon, SearchIcon, ViewIcon } from '@chakra-ui/icons';
import { FaBars, FaBell, FaPlus, FaRegUserCircle, FaTh } from 'react-icons/fa';
import {projectsQueries} from './_services/projects.queries';
import { useSuspenseQuery } from '@tanstack/react-query';
import axios from "axios";
import ProjectDetails from "../../../components/Project/ProjectDetails";
import { useState } from "react";


export const Route = createFileRoute("/_private/_projects/projets")({
  loader: ({ context: { queryClient } }) =>
    queryClient.ensureQueryData(projectsQueries()),
  component: Projects,
});


function Projects() {
  const {data:projects} = useSuspenseQuery(projectsQueries());
console.log("list of project", projects)


const navigate = useNavigate(); // Make sure this is correctly imported and used

const handleViewClick = (project) => {
  navigate({
    to: `/projets/${project.id}`,
    replace: false,  // Optional: Set to true to replace the current entry in the history stack
    resetScroll: false  // Optional: Set to true to reset the scroll position to the top
  });
};

const [showConfirmation, setShowConfirmation] = useState(false);
  const [projectIdToDelete, setProjectIdToDelete] = useState(null);

function handleDelete(projectId) {

  setShowConfirmation(true);
    setProjectIdToDelete(projectId);

}

const confirmDelete = (projectId) => {
  axios.delete(`http://localhost:5000/api/projects/deleteProject/${projectId}`)
    .then(response => {
      console.log(`Project with ID ${projectId} deleted successfully.`);
      window.location.reload();
      // Implement logic to update the UI if necessary (e.g., remove the deleted project from the table)
    })
    .catch(error => {
      console.error(`Error deleting project with ID ${projectId}:`, error.message);
      // Implement error handling logic if necessary
    });

  console.log(`Deleting project with ID: ${projectIdToDelete}`);
  setShowConfirmation(false);
};

const cancelDelete = () => {
  setShowConfirmation(false);
};


const columns = [
  {
    header: 'id',
    accessorKey: 'name',
    cell: props => {
      const value = props.getValue();
      console.log(value); // Place your console.log here
      return <span>{value ? value.toUpperCase() : ''}</span>;
    },  
  },
  {
    header: 'Estimated Start Date',
    accessorKey: 'estimatedStartDate',
    cell: props => <span>{props && props.getValue() ? new Date(props.getValue()).toLocaleDateString() : ''}</span>,
  },
  {
    header: 'Estimated End Date',
    accessorKey: 'estimatedEndDate',
    cell: props => <span>{props && props.getValue() ? new Date(props.getValue()).toLocaleDateString() : ''}</span>,
  },
  {
    header: 'Description',
    accessorKey: 'description',
  },
  {
    header: 'Status',
    accessorKey: 'status',
  },
  {
    header: 'Actions',
    accessorKey: 'actions',
    cell: props => (
      <>
        <ViewIcon
          onClick={() => handleViewClick(props.row.original)}
          _hover={{ color: 'blue' }}
          cursor="pointer"
          aria-label="View Project"
        />
        <DeleteIcon
          onClick={() => handleDelete(props.row.original._id)}
          _hover={{ color: 'red' }}
          cursor="pointer"
          aria-label="Delete Project"
        />
      </>
    ),
  },
];



  return (
    <div>
    <Box flex="1" p="4" mb="48" >

    <Flex justifyContent="space-between">
    <Flex flex="1" flexDirection="column"  >

      <Text fontSize="2xl" fontWeight="bold">
        Project List
      </Text>
      <Text fontSize="xs" color="gray.500" mb="12" fontWeight="semibold">
        Detailed information about your Projects
      </Text>
      </Flex>
    <Flex mt="2" align="start" flex="1" >
      <InputGroup>
        <InputLeftElement pointerEvents="none">
          <SearchIcon color="#CBD5E0" />
        </InputLeftElement>
        <Input type="search" placeholder="Search anything.." />
      </InputGroup>

      {/* Notification Icon */}
      <IconButton
        aria-label="Notification"
        icon={<FaBell />}
        size="md"
        variant="ghost"
        color="gray.400"
        ml={2}
        _hover={{ color: "#2563EB" }}
      />
      <IconButton
      aria-label="User"
      icon={<FaRegUserCircle/>}
      size="md"
      variant="ghost"
      color="#2563EB"
      ml={2}
      />
    </Flex>
  </Flex>
  <Flex justifyContent="space-between" align="center" mb="4">
    <Flex>
      <Button
        color="gray.500"
        border="1px"
        borderColor="gray.200"
        variant="ghost"
        rightIcon={<Icon as={ChevronDownIcon} />}
        mr="4"
      >
        Apply Filters
      </Button>
      <Button
        color="gray.500"
        border="1px"
        borderColor="gray.200"
        variant="ghost"
        rightIcon={<Icon as={ChevronDownIcon} />}
        mr="2"
      >
        All Projects
      </Button>
    </Flex>

    <Flex>
      <Link to = "/projects/addproject" > 
        <Button colorScheme="blue" leftIcon={<FaPlus />} mr="2"> 
          New Project 
        </Button> 
        </Link> 
      <Button 
        _hover={{ color: "#2563EB" }} 

        color="gray.300"
        variant="ghost"
      >
        <Icon as={FaTh} />
      </Button>
      <Button
        _hover={{ color: "#2563EB" }}
        color="gray.300"
        variant="ghost"
      >
        <Icon as={FaBars} />
      </Button>
    </Flex>
  </Flex>

  {/* <ul>
    {data.projects.map((project) => (
      <li key={project.id}>{project.name}</li>
    ))}
  </ul> */}
   {/* 
    {data.projects.map((project) => (
      <li key={project.id}>{project.name}</li>
    ))}
  */}

      <DataTable columns={columns} data={projects?projects  :[]} />
    </Box>

    {/* Confirmation Dialog */}
    {showConfirmation && (
  <Box
    position="fixed"
    top="50%"
    left="50%"
    transform="translate(-50%, -50%)"
    zIndex="9999"
    backgroundColor="gray.200"  // Updated to your main website color
    color="blue.900"
    p="4"
    width="400px"
    height="250px"
    borderRadius="lg"
    boxShadow="2xl"
    display="flex"
    flexDirection="column"
    alignItems="center"
    justifyContent="center"
  >
    <Text mb="4" fontSize="lg">Are you sure you want to delete this project?</Text>
    <Box mt={4} display="flex" gap={3}>
    <Button 
      leftIcon={<Icon as={CheckCircleIcon} />}  // Assuming you are using an icon library like 'react-icons'
      color="white"
      backgroundColor="red.400"
      _hover={{ color:"black" }}
      onClick={() => confirmDelete(projectIdToDelete)}  // Pass the project ID to the function
      mb="2"
    >
      Yes
    </Button>
    <Button 
      leftIcon={<Icon as={DeleteIcon} />}
      color="white"
      backgroundColor="green.400"
      _hover={{ color:"black" }}
      onClick={cancelDelete}
    >
      Cancel
    </Button>
    </Box>
  </Box>
)}
 

    </div>
  );
}

export default Projects;

