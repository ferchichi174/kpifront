import { CloseIcon, SearchIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Icon,
  IconButton,
  Input,
  InputGroup,
  InputLeftElement,
  Select,
  Stack,
  Text,
  Textarea,
  useToast,
} from "@chakra-ui/react";
import { Link, createLazyFileRoute } from "@tanstack/react-router";
import { FaBell, FaChevronLeft, FaPlus, FaRegUserCircle } from "react-icons/fa";
import { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export const Route = createLazyFileRoute(
    "/_private/_projects/projects/addproject"
)({
  component: () => {
    const [data, setData] = useState([]);
    const [data1, setData1] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [formData, setFormData] = useState({
      id: "", 
      selectedOption: "",
      estimatedStartDate: "",
      estimatedEndDate: "",
      description: "",
      name:""
    });

    { /*  Success and echec toast notifications */}
    const [showSuccessToast, setShowSuccessToast] = useState(false);
    const [showEchecToast, setShowEchecToast] = useState(false);
    const [showExistingProjectToast, setShowExistingProjectToast] = useState(false);

    const navigate = useNavigate();


  {/*  Fetch data from the server when the component mounts  */}
    { /* will be used in Select projects from clickUp  */}
    useEffect(() => {
      const fetchData = async () => {
        try {
          const response = await axios.get("https://middleware-clickup.onrender.com/api/v1/folders/");
          setData(response.data);
        //  console.log("hethi list mtaa projects clickUp elli famma ", response.data)

        } catch (error) {
          setError(error.message);
        } finally {
          setLoading(false);
        }
      };

      fetchData();
    }, []);

   // console.log("outside effect hethi list mtaa projects clickUp elli famma ", data1)


    ///// hetha chfamma f list des projet // to don't allow adding existing projects
    useEffect(() => {
      const fetchData = async () => {
        try {
          const response = await axios.get("http://localhost:5000/api/projects/getProject");
          setData1(response.data);
         // console.log("hethi list mtaa projects mongoDB elli famma ", response.data)
         // console.log("hetha chfamma fi formData",formData)

        } catch (error) {
          setError(error.message);
        } finally {
          setLoading(false);
        }
      };

      fetchData();
    }, []);


    const handleChange = (e, field) => {
      const selectedName = e.target.value;
      const selectedItem = data.find(item => item.name === selectedName);
      if (field === 'selectedOption') {
        setFormData(prevFormData => ({
          ...prevFormData,
          [field]: selectedName,  // Ceci met à jour l'option sélectionnée avec le nom du projet
          name: selectedName,     // Assurez-vous de mettre à jour le nom ici également
          id: selectedItem ? selectedItem.id : "",
          _id: selectedItem ? selectedItem._id : ""
        }));
      } else {
        setFormData(prevFormData => ({
          ...prevFormData,
          [field]: selectedName
        }));
      }
    };


    const handleSubmit = async (e) => {
      e.preventDefault();
      if (!formData._id || !formData.estimatedStartDate || !formData.estimatedEndDate || !formData.description) {
        console.log("Please complete all required fields.");
        setShowEchecToast(true);
      } else {
        try {
          const response = await axios.post("http://localhost:5000/api/projects/addProject", {
            ...formData,
            id: formData._id  // Utilisation de _id comme identifiant principal pour le nouveau projet
          });
          console.log("Project added successfully:", response.data);
          setShowSuccessToast(true);
          setFormData({
            id: "",
            _id: "", // Assurez-vous de réinitialiser _id aussi
            selectedOption: "",
            estimatedStartDate: "",
            estimatedEndDate: "",
            description: "",
            name: ""
          });
        } catch (error) {
          console.error("Error adding project:", error);
          setShowEchecToast(true);
        }
      }
    };

    const handleClearForm = () => {
      setFormData({
        id: "", 
        selectedOption: "",
        estimatedStartDate: "",
        estimatedEndDate: "",
        description: "",
        name:""
      });
    };

    return (
        <div>
          <Box>
            <Flex justifyContent="space-between">
              <Flex flex="1" flexDirection="column">
                <Text fontSize="2xl" fontWeight="bold">
                  Project List
                </Text>
                <Text
                    fontSize="xs"
                    color="gray.500"
                    mb="12"
                    fontWeight="semibold"
                >
                  Detailed information about your Projects
                </Text>
              </Flex>
              <Flex mt="2" align="start" flex="1">
                <InputGroup>
                  <InputLeftElement pointerEvents="none">
                    <SearchIcon color="gray.500" />
                  </InputLeftElement>
                  <Input type="search" placeholder="Search anything.." />
                </InputGroup>

                <IconButton
                    aria-label="Notification"
                    icon={<FaBell />}
                    size="md"
                    variant="ghost"
                    color="gray.400"
                    ml={2}
                    _hover={{ color: "#2563EB" }}
                />
                <IconButton
                    aria-label="User"
                    icon={<FaRegUserCircle />}
                    size="md"
                    variant="ghost"
                    color="#2563EB"
                    ml={2}
                />
              </Flex>
            </Flex>
          </Box>

          <Box bg="gray.50" p="4">
            <Box
                bg="white"
                width="750px"
                mx="auto"
                boxShadow="xs"
                p="8"
                paddingX="24"
                borderWidth="1px"
                borderRadius="lg"
            >
              <Stack spacing="4">
                <Box>
                  <Box fontWeight="bold" fontSize="xl" mb="4" textAlign="center">
                    Add Project
                  </Box>
                  <FormControl mt="4">
                    <FormLabel>Select Project ID (ClickUp)</FormLabel>
                    <Select
                        value={formData.selectedOption}
                        onChange={(e) => handleChange(e, "selectedOption")}
                    >
                      <option value="">Select Project ID (ClickUp)</option>
                      {data.map(item => (
                          <option key={item.id} value={item.name}>
                            {item.name}
                          </option>
                      ))}
                    </Select>
                  </FormControl>
                </Box>
                <Box mt="4">
                  <Stack direction="row" spacing="4">
                    <FormControl flex="1">
                      <FormLabel>Estimated Start Date</FormLabel>
                      <Input
                          value={formData.estimatedStartDate}
                          onChange={(e) =>
                              handleChange(e, "estimatedStartDate")
                                }
                          borderColor="blue.200"
                          borderRadius="lg"
                          type="date"
                          placeholder="Estimated Start Date"
                      />
                    </FormControl>
                    <FormControl flex="1">
                      <FormLabel>Estimated End Date</FormLabel>
                      <Input
                          value={formData.estimatedEndDate}
                          onChange={(e) =>
                              handleChange(e, "estimatedEndDate")
                          }
                          borderColor="blue.200"
                          borderRadius="lg"
                          type="date"
                          placeholder="Estimated End Date"
                      />
                    </FormControl>
                  </Stack>
                </Box>
                <Box mt="4">
                  <FormControl>
                    <FormLabel>Description</FormLabel>
                    <Textarea
                        value={formData.description}
                        onChange={(e) => handleChange(e, "description")}
                        borderColor="blue.200"
                        borderRadius="lg"
                        placeholder="Description"
                        size="lg"
                        height="200px"
                    />
                  </FormControl>
                </Box>
                <Box mt="8">
                  <Stack
                      direction="row"
                      spacing="4"
                      justify="space-between"
                  >
                   <Link to="/projets">
                    <Button
                        leftIcon={<Icon as={FaChevronLeft} />}
                        colorScheme="blue"
                    >
                      Previous
                    </Button>
                 </Link>
                    <Stack direction="row" spacing="4">
                      <Button
                          leftIcon={<CloseIcon />}
                          colorScheme="gray"
                          onClick={handleClearForm}
                      >
                        Clear Form
                      </Button>
                      <Button
                          onClick={handleSubmit}
                          leftIcon={<Icon as={FaPlus} />}
                          colorScheme="blue"
                      >
                        Add Project
                      </Button>
                    </Stack>
                  </Stack>
                </Box>
              </Stack>
            </Box>
          </Box>
                 {/*   Success Toast notification */}
          {showSuccessToast && (
              <Box
                  position="fixed"
                  top="20px"
                  right="20px"
                  zIndex="9999"
                  backgroundColor="green.500"
                  color="white"
                  p="4"
                  borderRadius="md"
                  boxShadow="lg"
              >
                Project added successfully!
              </Box>
          )}
                 {/*   Echec Toast notification */}
                 {showEchecToast && (
              <Box
                  position="fixed"
                  top="20px"
                  right="20px"
                  zIndex="9999"
                  backgroundColor="red.500"
                  color="white"
                  p="4"
                  borderRadius="md"
                  boxShadow="lg"
              >
                Please complete all fields!
              </Box>
          )}

                 {/*   Existing project Toast notification */}


          {showExistingProjectToast && (
              <Box
                  position="fixed"
                  top="20px"
                  right="20px"
                  zIndex="9999"
                  backgroundColor="blue.500"
                  color="white"
                  p="4"
                  borderRadius="md"
                  boxShadow="lg"
              >
                Can't add Project, it already exists!
              </Box>
          )}   

        </div>
    );
  },
});
