import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useMatch, createFileRoute } from '@tanstack/react-router';
import {
  Box,
  Flex,
  Button,
  Tabs,
  TabList,
  TabPanel,
  TabPanels,
  Tab,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  VStack,
  Input,
  Textarea,
  Select,
  useDisclosure,
  Grid, Divider
} from '@chakra-ui/react';
import ProjectDetails from '../../../../components/Project/ProjectDetails';
import DataTable from '../../../../components/table/DataTable';

export const Route = createFileRoute('/_private/_projects/_projectDetails/projets/$id')({
  component: () => {
    const match = useMatch({ to: 'projets/:idProjet' });
    const idProjet = match?.params?.id; // Ensure it's captured correctly from the route
    const [project, setProject] = useState({
      name: '',
      id: '',
      estimatedStartDate: '',
      estimatedEndDate: '',
      description: ''
    });
    const [tasks, setTasks] = useState([]);
    const [users, setUsers] = useState([]);
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [newTask, setNewTask] = useState({
      name: '',
      description: '',
      dueDate: '',
      status: 'pending'
    });
    const [tabIndex, setTabIndex] = useState(0);


    useEffect(() => {
      const fetchProject = async () => {
        try {
          // Adjust the URL to your needs
          const response = await axios.get(`http://localhost:5000/api/projects/${idProjet}`);
          setProject({
            name: response.data.name,
            id: response.data._id, // Assuming your project data includes an `_id` field
            estimatedStartDate: response.data.estimatedStartDate,
            estimatedEndDate: response.data.estimatedEndDate,
            description: response.data.description
          });
        } catch (error) {
          console.error('Error fetching project details:', error);
        }
      };

      fetchProject();
    }, []);
    useEffect(() => {


      const fetchProject = async () => {
        try {
          const response = await axios.get(`http://localhost:5000/api/projects/getProject/${idProjet}`);
          setProject(response.data);
        } catch (error) {
          console.error('Error fetching project details:', error);
          // Consider setting an error state here and show it in UI
        }
      };
      fetchProject();
    }, [idProjet]); // Correct dependency

    useEffect(() => {
      const controller = new AbortController(); // To cancel fetch on component unmount
      if (tabIndex === 0) {
        const fetchUsers = async () => {
          try {
            const response = await axios.get(`https://middleware-clickup.onrender.com/api/v1/folders/${idProjet}`, { signal: controller.signal });
            console.log("API response:", response.data); // Logs the complete response data

            // Initialize an empty array to store all members from all lists
            let allMembers = [];

            // Iterate over each list and extract members if available
            response.data.lists.forEach((list) => {
              console.log(`Details of list:`, list);

              if (list.members && Array.isArray(list.members)) {
                allMembers = allMembers.concat(list.members.map(member => ({
                  _id: member._id,
                  username: member.email ? member.email.split('@')[0] : 'Unknown', // Assuming members have an email and creating username from it
                  email: member.email || 'No email provided', // Provide default value if email is missing
                  profilePicture: member.profilePicture || "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                })));
              }
            });

            // Update state with all collected members
            if (allMembers.length > 0) {
              setUsers(allMembers);
            } else {
              console.error("No member data found within lists.");
            }

          } catch (error) {
            if (!controller.signal.aborted) {
              console.error('Failed to fetch users', error);
            }
          }
        };

        fetchUsers();
      } else if (tabIndex === 1) {

        console.log(idProjet)
        getTasksFromFolder(idProjet);
      }
      return () => controller.abort(); // Cleanup function to abort fetch
    }, [tabIndex, idProjet]);

    const getTasksFromFolder = async (idProjet) => {
      try {
        const response = await axios.get('https://middleware-clickup.onrender.com/api/v1/tasks/');
        const filteredTasks = response.data.filter(task => task.folder === idProjet);
        setTasks(filteredTasks.map(task => ({
          name: task.name,
          description: task.description
        })));
      } catch (error) {
        console.error('Error fetching tasks:', error);
      }
    };

    const handleInputChange = (e) => {
      const { name, value } = e.target;
      setNewTask(prev => ({ ...prev, [name]: value }));
    };

    const handleCreateTask = async () => {
      try {
        const response = await axios.post(`http://localhost:5000/api/tasks/addTask`, {
          ...newTask,
          projectId: idProjet
        });
        setTasks(prevTasks => [...prevTasks, response.data]);
        onClose();
        setNewTask({ name: '', description: '', dueDate: '', status: 'pending' }); // Reset form
      } catch (error) {
        console.error('Failed to create task', error);
      }
    };

    const userColumns = [
      {
        header: 'Profile Picture',
        accessorKey: 'profilePicture',
        Cell: ({ value }) => (
            <img src={value} alt="Profile" style={{ width: '50px', height: '50px', borderRadius: '25px' }} />
        )
      },
      { header: 'User ID', accessorKey: '_id' },
      { header: 'Username', accessorKey: 'username' },
      { header: 'Email', accessorKey: 'email' },

    ];



    const taskColumns = [
      { header: 'Name', accessorKey: 'name' },
      { header: 'Description', accessorKey: 'description' },
      // { header: 'Status', accessorKey: 'status' }
    ];
    return (
        <>

          <ProjectDetails
              projectName={project.name}
              projectId={project.id}
              estimatedStartDate={new Date(project.estimatedStartDate).toLocaleDateString()}
              estimatedEndDate={new Date(project.estimatedEndDate).toLocaleDateString()}
              description={project.description}
          />


          <Tabs variant='soft-rounded' colorScheme='blue' onChange={index => setTabIndex(index)}>
            <TabList mb="1em">
              <Tab>Users</Tab>
              <Tab>Tasks</Tab>
            </TabList>
            <TabPanels>
              <TabPanel>
                <DataTable columns={userColumns} data={users} />
              </TabPanel>
              <TabPanel>
                <Flex justifyContent="flex-end" mb={4}>
                  <Button colorScheme="blue" onClick={onOpen}>Create New Task</Button>
                </Flex>
                <DataTable columns={taskColumns} data={tasks} />
                <Modal isOpen={isOpen} onClose={onClose}>
                  <ModalOverlay />
                  <ModalContent>
                    <ModalHeader>Create New Task</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                      <VStack spacing={4}>
                        <Input placeholder="Task Name" name="name" value={newTask.name} onChange={handleInputChange} />
                        <Textarea placeholder="Description" name="description" value={newTask.description} onChange={handleInputChange} />
                        <Input type="date" name="dueDate" value={newTask.dueDate} onChange={handleInputChange} />
                        <Select name="status" value={newTask.status} onChange={handleInputChange}>
                          <option value="pending">Pending</option>
                          <option value="ongoing">Ongoing</option>
                          <option value="completed">Completed</option>
                        </Select>
                      </VStack>
                    </ModalBody>
                    <ModalFooter>
                      <Button colorScheme="blue" mr={3} onClick={handleCreateTask}>Save</Button>
                      <Button onClick={onClose}>Cancel</Button>
                    </ModalFooter>
                  </ModalContent>
                </Modal>
              </TabPanel>
            </TabPanels>
          </Tabs>
        </>
    );
  }
});