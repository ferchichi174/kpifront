import { createFileRoute } from '@tanstack/react-router'
import { useState } from 'react';
import { Flex, Box, Text, Table, Thead, Tbody, Tr, Th, Td, Input, Icon } from '@chakra-ui/react';
import { AtSignIcon, CheckIcon, EditIcon, UpDownIcon } from '@chakra-ui/icons';


export const Route = createFileRoute('/_private/_Dashboard/Dashboard')({
  component: () => {
    
    const data = [
      { title: 'Box 1', value: 'Value 1', icon: UpDownIcon },
      { title: 'Box 2', value: 'Value 2', icon: AtSignIcon },
      { title: 'Box 3', value: 'Value 3', icon: EditIcon },
      { title: 'Box 4', value: 'Value 4', icon: CheckIcon },
    ];

  return (
    <Flex direction="row" justify="space-between" padding="4" wrap="wrap">
    {data.map((item, index) => (
      <Box
        key={index}
        flex="1"
        minW="200px"
        margin="2"
        padding="4"
        bg="gray.100"
        _hover={{ bg: "gray.50" }}        borderRadius="md"
        boxShadow="md"
      >
        <Flex align="center" mb="2">
          <Icon as={item.icon} color="blue.500" mr="2" />
          <Text fontSize="2xl" fontWeight="bold" color="blue.500">
            {item.title}
          </Text>
        </Flex>
        <Text fontWeight="medium" color="black">
          {item.value}
        </Text>
      </Box>
    ))}
  </Flex>
  );
}

  })