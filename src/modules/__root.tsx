import { QueryClient } from "@tanstack/react-query";
import {
  Outlet,
  ScrollRestoration,
  createRootRouteWithContext,
} from "@tanstack/react-router";
import { TanStackRouterDevtools } from "@tanstack/router-devtools";

export const Route = createRootRouteWithContext<{
  queryClient: QueryClient
}>()({
  component: RootComponent,
})
function RootComponent() {
  return (
    <div>
      <ScrollRestoration />

      <TanStackRouterDevtools position='bottom-right' />

      <Outlet />
    </div>
  );
}
export default RootComponent;
