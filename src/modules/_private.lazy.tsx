import { createFileRoute } from "@tanstack/react-router";
import Layout from "../layout/Layout";

export const Route = createFileRoute("/_private")({
  component: Private,
  beforeLoad: () => {
    // if(!localStorage.getItem('token')) {
    //   throw redirect({
    //     to: '/auth/login',
    //   })    }
  },
});
function Private() {
  return (
    <div>
      <Layout />
    </div>
  );
}
