import React, {useEffect, useState} from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Box, Flex, Heading, Image, Checkbox, Button, Link, Text } from '@chakra-ui/react';
import { EmailIcon, LockIcon } from '@chakra-ui/icons';
import TextInput from '../../../components/form/TextInput';
import BtnForm from '../../../components/form/BtnForm';
import useValitBotForrm from '../../../components/form/useForm';
import { email, minLength, string } from 'valibot';
import logo from '../../../images/whiteLogo.png';
import loginImage from '../../../images/loginImage.png';
import {createFileRoute} from "@tanstack/react-router";

export const Route = createFileRoute("/_public/auth/login")({
  component: Login,
});

function Login() {
  const [error, setError] = useState('');
  const navigate = useNavigate();
  const { Field, Subscribe, handleSubmit } = useValitBotForrm({
    defaultValues: {
      email: "",
      password: "",
    },
    onSubmit: async ({ value }) => {
      try {
        const response = await axios.post('http://localhost:5000/auth/login', value);
        if (response.data.token) {
          localStorage.setItem('token', response.data.token);
          window.location.href = '/projets';
        } else {
          setError('No token received, please check your credentials.');
        }
      } catch (error) {
        console.error("Erreur lors de la connexion :", error);
        setError('Failed to login: ' + (error.response?.data?.message || 'Unknown error'));
      }
    },
  });

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      window.location.href = '/projets';
    }
  }, [navigate]);

  const [rememberMe, setRememberMe] = useState(false);
  const handleRememberMeChange = () => setRememberMe(!rememberMe);

  return (
      <div>
        <Flex>
          <Box flex="1" bg="#2563EB" color="white" p="8" textAlign="center">
            <Flex align="center">
              <Image src={logo} boxSize="32px" mr="2" />
              <Heading size="lg" textAlign="left">KPI Project</Heading>
            </Flex>
            <Image src={loginImage} p="12" m="24" mx="auto" />
          </Box>

          <Box flex="1" m="24" pl="24" pr="24">
            <Heading as="h2" size="md" mb="1" textAlign="left">Sign In to your Account</Heading>
            <Text textAlign="left" fontSize="xs" color="gray.500">
              Welcome back! Please enter your details.
            </Text>

            {error && <Text color="red.500" textAlign="center" fontSize="md" p="4">{error}</Text>}

            <TextInput
                Field={Field}
                leftIcon={<EmailIcon color="#CBD5E0"/>}
                name="username"
                type="text"
                placeholder="username"

            />

            <TextInput
                leftIcon={<LockIcon color="#CBD5E0"/>}
                validators={{
                  onChange: string([
                    minLength(6, "Password must be at least 6 characters"),
                  ]),
                }}
                Field={Field}
                name="password"
                type="password"
                placeholder="Password"
            />

            <Flex align="center" mt="4">
              <Checkbox colorScheme="blue" isChecked={rememberMe} onChange={handleRememberMeChange}>Remember me</Checkbox>
              <Box ml="auto">
                <Button colorScheme="blue" variant="link">Forgot password?</Button>
              </Box>
            </Flex>

            <BtnForm
                Subscribe={Subscribe}
                name="submit"
                handleSubmit={handleSubmit}
                width="full"
                marginTop="6"
            />
          </Box>
        </Flex>
      </div>
  );
}

export default Login;
